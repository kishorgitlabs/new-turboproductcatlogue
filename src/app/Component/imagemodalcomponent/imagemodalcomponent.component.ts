import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-imagemodalcomponent',
  templateUrl: './imagemodalcomponent.component.html',
  styleUrls: ['./imagemodalcomponent.component.scss'],
})
export class ImagemodalcomponentComponent implements OnInit {

  
  @ViewChild(IonSlides) slides: IonSlides;
  sliderOpts = {
    zoom: true
  };
  imgs;
  constructor(
    private navParams: NavParams,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.imgs = this.navParams.data.img;
  }
  ionViewDidEnter() {
    this.slides.update();
  }

  async zoom(zoomIn: boolean) {
    const slider = await this.slides.getSwiper();
    const zoom = slider.zoom;
    zoomIn ? zoom.in() : zoom.out();
  }

  close() {
    this.modalController.dismiss();
  }
}
