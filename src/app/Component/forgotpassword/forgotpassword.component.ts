import { ConfigService } from 'src/app/Services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss'],
})
export class ForgotpasswordComponent implements OnInit {

  ngMail;
  constructor
  (
    private modalCtrl:ModalController,
    private configservice:ConfigService
  ) {  }

  ngOnInit() {
    
  }

  async submit() {
    console.log(this.ngMail);
    if (this.ngMail === null || this.ngMail === undefined || this.ngMail === '') {
      this.configservice.toastfn(`Please enter your email address`);
      return;
    }
    const values = {
      "email": this.ngMail
    }
    this.configservice.loader('Loading...');
    this.configservice.postData('api/mobile/forgotpswd', values).subscribe((res) => {
      let response: any = res;
      if (response.result === true) {
       
        this.configservice.loaderDismiss();
        this.modalCtrl.dismiss();
        this.configservice.toastfn(response.msg);
       
      }
      else if (response.result === false) {
        this.configservice.loaderDismiss();
        this.configservice.toastfn(response.msg);
      }
      else {
        this.configservice.loaderDismiss();
        this.configservice.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.configservice.loaderDismiss();
      this.configservice.alertService('Something went wrong . Please try again later .');
    });
  }


}
