import { ConfigService } from 'src/app/Services/config/config.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ForgotpasswordComponent } from '../forgotpassword/forgotpassword.component';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
  username: string;
  password: string;
 getMobNo:any;

  constructor(
    private modalCtrl: ModalController,
    private formbuilder: FormBuilder,
    private config: ConfigService,
    private modalctrl:ModalController
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {  
this.getMobNo=JSON.parse(localStorage.getItem('UserDetail'));

  }

  async close() {
    this.modalCtrl.dismiss();
  }

  async onFormSubmit(value) {
    if (value.username === '') {
      this.config.toastfn('Enter your user name');
    }
    else if (value.password === '') {
      this.config.toastfn('Enter your password');
    }
    else {
 
      this.checkUserLogin(value);

    }
  
  }

  async checkUserLogin(value) {

    const values = {
     "username":value.username,
     "password":value.password
    }

    this.config.loader('Loading...');
    this.config.postData('api/mobile/userlogin', values).subscribe((res) => {

      let response: any = res;

      if (response.result === true) {
        this.config.loaderDismiss();
        this.loginForm.reset();
        localStorage.setItem('lsLogin','true')
        this.config.alertService('Login Successfully');
        this.modalCtrl.dismiss();
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        this.loginForm.reset();
        this.config.alertService('Login Failed.Please Enter Correct User Name or Password');
      }
      else {
        this.config.loaderDismiss();
        this.loginForm.reset();
        this.config.alertService('Something went wrong . Please try again later .');
      }

    }, err => {
      this.config.loaderDismiss();
      this.loginForm.reset();
      this.config.alertService('Something went wrong . Please try again later .');
    });
  }


  async forgotpassword(){

    const modalCreate =this.modalCtrl.create({
      component:ForgotpasswordComponent,
      cssClass:'alertclass'
      
    });
    await (await modalCreate).present();

  }

}

