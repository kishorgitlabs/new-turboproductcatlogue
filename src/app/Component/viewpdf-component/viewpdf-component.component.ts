import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
const ZOOM_STEP: number = 0.25;
const DEFAULT_ZOOM: number = 1;
@Component({
  selector: 'app-viewpdf-component',
  templateUrl: './viewpdf-component.component.html',
  styleUrls: ['./viewpdf-component.component.scss'],
})
export class ViewpdfComponentComponent implements OnInit {


  url;
  title = 'PDF';
  isShow: boolean = true;
  public pdfZoom: number = DEFAULT_ZOOM;

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams,
  ) { }

  ngOnInit() {
    this.title = this.navParams.data.title;
    if (this.navParams.data.mode === 'local') {
      this.url = this.navParams.data.url;
    } else {
      this.url = `https://corsbm.onrender.com/${this.navParams.data.url}`;
    }
    // alert(this.url);
    setTimeout(() => {
      this.isShow = false;
    }, 20000);
  }

  async close() {
    this.modalCtrl.dismiss();
  }

  onProgress(e) {
    console.log('on-progress', e);
    this.isShow = false;
  }

  zoomIn() {
    this.pdfZoom += ZOOM_STEP;
  }

  zoomOut() {
    if (this.pdfZoom > DEFAULT_ZOOM) {
      this.pdfZoom -= ZOOM_STEP;
    }
  }

  resetZoom() {
    this.pdfZoom = DEFAULT_ZOOM;
  }



}
