import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {

  alertName: string;

  constructor(
    private navparams: NavParams,
    private config: ConfigService,
    private router: Router,
    private modalctrl: ModalController
  ) { }

  ngOnInit() {
    this.alertName = this.navparams.data.alertName;
  }

  async okay(value) {

    if (value === 'Network Disconnected') {
      this.config.networkExit();
      this.modalctrl.dismiss();
    }
    else if (value === 'Your device is not matched .Please contact turbo team .') {
      this.modalctrl.dismiss();
      this.config.networkExit();
    }
    else if(value === 'Device does not match'){
      this.modalctrl.dismiss();
      this.config.networkExit();
    }
    else if (value === 'Please wait for Admin Approval') {
      this.router.navigate(['/dashboard']);
      this.modalctrl.dismiss();
    }
    else if (value === 'Registration Completed') {
      this.router.navigate(['/dashboard']);
      this.modalctrl.dismiss();
    }
    else if (value === 'Login Failed.Please Enter Correct User Name or Password') {
      this.modalctrl.dismiss();
    }
    else if (value === 'Login Successfully') {
      this.modalctrl.dismiss();
      this.router.navigate(['/dashboard']);
    }
    else   if (value === 'Your account is deactivated . Please contact turbo team .') {
      this.config.networkExit();
      this.modalctrl.dismiss();
    }
    else if (value === 'Something went wrong . Please try again later .') {
      this.modalctrl.dismiss();
    }
    else if (value === 'Your OTP is incorrect .Please enter correct otp .') {
      this.modalctrl.dismiss();
    }
    else if (value === 'No Details Found .') {
      this.modalctrl.dismiss();
    }
    else if (value === 'Feedback has been sent to turbo team .') {
      this.modalctrl.dismiss();
    }
    else if (value === 'Profile Updated Successfully .' || value === 'Partno doesnot match') {
      this.modalctrl.dismiss();
      
    }
  }

}
