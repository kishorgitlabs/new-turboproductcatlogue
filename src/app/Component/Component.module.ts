import { LoginComponent } from './login/login.component';
import { Header1Component } from './header1/header1.component';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AlertComponent } from "./alert/alert.component";
import { ImagemodalcomponentComponent } from './imagemodalcomponent/imagemodalcomponent.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ViewpdfComponentComponent } from './viewpdf-component/viewpdf-component.component';
import { PdfViewerModule } from 'ng2-pdf-viewer'; 

@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        AlertComponent,
        Header1Component,
        LoginComponent,
        ImagemodalcomponentComponent,
        ForgotpasswordComponent,
        ViewpdfComponentComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PdfViewerModule
    ],
    exports: [
        HeaderComponent,
        FooterComponent,
        AlertComponent,
        Header1Component,
        LoginComponent,
        ImagemodalcomponentComponent,
        ForgotpasswordComponent,
        ViewpdfComponentComponent
    ]
})
export class ComponentModule { }