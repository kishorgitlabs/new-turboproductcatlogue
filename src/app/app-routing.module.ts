import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { GuardGuard } from './Services/guard/guard.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'register',
    loadChildren: () => import('./Pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./Pages/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate:[GuardGuard]
  },
 
  
  {
    path: 'make',
    loadChildren: () => import('./Pages/Manufacturer/make/make.module').then( m => m.MakePageModule)
  },
  {
    path: 'makesegment',
    loadChildren: () => import('./Pages/Manufacturer/makesegment/makesegment.module').then( m => m.MakesegmentPageModule)
  },
  {
    path: 'segment',
    loadChildren: () => import('./Pages/SegmentFlow/segment/segment.module').then( m => m.SegmentPageModule)
  },
  {
    path: 'segmentmake',
    loadChildren: () => import('./Pages/SegmentFlow/segmentmake/segmentmake.module').then( m => m.SegmentmakePageModule)
  },
  {
    path: 'segmentsubsegment',
    loadChildren: () => import('./Pages/SegmentFlow/segmentsubsegment/segmentsubsegment.module').then( m => m.SegmentsubsegmentPageModule)
  },
  {
    path: 'segmentpartdetails',
    loadChildren: () => import('./Pages/SegmentFlow/segmentpartdetails/segmentpartdetails.module').then( m => m.SegmentpartdetailsPageModule)
  },
  {
    path: 'segmentapplication',
    loadChildren: () => import('./Pages/SegmentFlow/segmentapplication/segmentapplication.module').then( m => m.SegmentapplicationPageModule)
  },
  {
    path: 'application',
    loadChildren: () => import('./Pages/applicationflow/application/application.module').then( m => m.ApplicationPageModule)
  },
  {
    path: 'applicationpartdetails',
    loadChildren: () => import('./Pages/applicationflow/applicationpartdetails/applicationpartdetails.module').then( m => m.ApplicationpartdetailsPageModule)
  },
  {
    path: 'applicationenginespecs',
    loadChildren: () => import('./Pages/applicationflow/applicationenginespecs/applicationenginespecs.module').then( m => m.ApplicationenginespecsPageModule)
  },
  {
    path: 'makesubsegment',
    loadChildren: () => import('./Pages/Manufacturer/makesubsegment/makesubsegment.module').then( m => m.MakesubsegmentPageModule)
  },
  {
    path: 'makeapplication',
    loadChildren: () => import('./Pages/Manufacturer/makeapplication/makeapplication.module').then( m => m.MakeapplicationPageModule)
  },
  {
    path: 'makepartdetails',
    loadChildren: () => import('./Pages/Manufacturer/makepartdetails/makepartdetails.module').then( m => m.MakepartdetailsPageModule)
  },

  {
    path: 'search',
    loadChildren: () => import('./Pages/search/search.module').then( m => m.SearchPageModule)
  },
  {
    path: 'network',
    loadChildren: () => import('./Pages/network/network.module').then( m => m.NetworkPageModule)
  },
 
  {
    path: 'whatsnew',
    loadChildren: () => import('./Pages/whatsnew/whatsnew.module').then( m => m.WhatsnewPageModule)
  },
  {
    path: 'contactus',
    loadChildren: () => import('./Pages/contactus/contactus.module').then( m => m.ContactusPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./Pages/notification/notification.module').then( m => m.NotificationPageModule)
  },

  {
    path: 'vidoepdf',
    loadChildren: () => import('./Pages/Others/vidoepdf/vidoepdf.module').then( m => m.VidoepdfPageModule)
  },
  {
    path: 'videos',
    loadChildren: () => import('./Pages/Others/videos/videos.module').then( m => m.VideosPageModule)
  },
  {
    path: 'pdf',
    loadChildren: () => import('./Pages/Others/pdf/pdf.module').then( m => m.PdfPageModule)
  },
  {
    path: 'searchpartno',
    loadChildren: () => import('./Pages/searchpartno/searchpartno.module').then( m => m.SearchpartnoPageModule)
  },
  {
    path: 'searchkitno',
    loadChildren: () => import('./Pages/searchkitno/searchkitno.module').then( m => m.SearchkitnoPageModule)
  },
  {
    path: 'product',
    loadChildren: () => import('./Pages/Productcatlogue/product/product.module').then( m => m.ProductPageModule)
  },
  {
    path: 'allmakeoemname',
    loadChildren: () => import('./Pages/Productcatlogue/AllMake/allmakeoemname/allmakeoemname.module').then( m => m.AllmakeoemnamePageModule)
  },
  {
    path: 'allmakeapplication',
    loadChildren: () => import('./Pages/Productcatlogue/AllMake/allmakeapplication/allmakeapplication.module').then( m => m.AllmakeapplicationPageModule)
  },
  {
    path: 'remanmake',
    loadChildren: () => import('./Pages/Productcatlogue/Remanfacturer/remanmake/remanmake.module').then( m => m.RemanmakePageModule)
  },
  {
    path: 'remansegment',
    loadChildren: () => import('./Pages/Productcatlogue/Remanfacturer/remansegment/remansegment.module').then( m => m.RemansegmentPageModule)
  },
  {
    path: 'remansubsegment',
    loadChildren: () => import('./Pages/Productcatlogue/Remanfacturer/remansubsegment/remansubsegment.module').then( m => m.RemansubsegmentPageModule)
  },
  {
    path: 'remanapplication',
    loadChildren: () => import('./Pages/Productcatlogue/Remanfacturer/remanapplication/remanapplication.module').then( m => m.RemanapplicationPageModule)
  },
  {
    path: 'pricelists',
    loadChildren: () => import('./Pages/PriceListFlows/pricelists/pricelists.module').then( m => m.PricelistsPageModule)
  },
  {
    path: 'pricelists',
    loadChildren: () => import('./Pages/PriceListFlows/pricelists/pricelists.module').then( m => m.PricelistsPageModule)
  },
  {
    path: 'pricelistpartdetails',
    loadChildren: () => import('./Pages/PriceListFlows/pricelistpartdetails/pricelistpartdetails.module').then( m => m.PricelistpartdetailsPageModule)
  },
  {
    path: 'whatsnewpartdetails',
    loadChildren: () => import('./Pages/whatsnewpartdetails/whatsnewpartdetails.module').then( m => m.WhatsnewpartdetailsPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./Pages/SettingsFlow/setting/setting.module').then( m => m.SettingPageModule)
  },
  {
    path: 'editprofile',
    loadChildren: () => import('./Pages/SettingsFlow/editprofile/editprofile.module').then( m => m.EditprofilePageModule)
  },
  {
    path: 'pdfdetails',
    loadChildren: () => import('./Pages/Others/pdfdetails/pdfdetails.module').then( m => m.PdfdetailsPageModule)
  },
  {
    path: 'videodetails',
    loadChildren: () => import('./Pages/Others/videodetails/videodetails.module').then( m => m.VideodetailsPageModule)
  },
  {
    path: 'segmentpartnodetails',
    loadChildren: () => import('./Pages/segmentpartnodetails/segmentpartnodetails.module').then( m => m.SegmentpartnodetailsPageModule)
  },
  {
    path: 'searchkitnopartdetails',
    loadChildren: () => import('./Pages/searchkitnopartdetails/searchkitnopartdetails.module').then( m => m.SearchkitnopartdetailsPageModule)
  },
  {
    path: 'applicationoem',
    loadChildren: () => import('./Pages/applicationflow/applicationoem/applicationoem.module').then( m => m.ApplicationoemPageModule)
  },







 

  
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
