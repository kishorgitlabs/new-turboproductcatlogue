import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SegmentpartdetailsPageRoutingModule } from './segmentpartdetails-routing.module';

import { SegmentpartdetailsPage } from './segmentpartdetails.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SegmentpartdetailsPageRoutingModule,
    ComponentModule
  ],
  declarations: [SegmentpartdetailsPage]
})
export class SegmentpartdetailsPageModule {}
