import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SegmentapplicationPageRoutingModule } from './segmentapplication-routing.module';

import { SegmentapplicationPage } from './segmentapplication.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SegmentapplicationPageRoutingModule,
    ComponentModule
  ],
  declarations: [SegmentapplicationPage]
})
export class SegmentapplicationPageModule {}
