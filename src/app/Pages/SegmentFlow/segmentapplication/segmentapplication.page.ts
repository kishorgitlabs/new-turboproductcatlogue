import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-segmentapplication',
  templateUrl: './segmentapplication.page.html',
  styleUrls: ['./segmentapplication.page.scss'],
})
export class SegmentapplicationPage implements OnInit {

  
  segmentApplicationJson = [];
  makeName: string;
  segmentName: string;
  subSegmentName: string;
  isSegmentTable:boolean=false;

  constructor(
    private activateroute: ActivatedRoute,
    public config: ConfigService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res) => {
      const obj = JSON.parse(res.segmentMakeObj);
      console.log(obj);
      this.segmentName = obj.segment;
      this.subSegmentName = obj.subsegment;
      this.makeName = obj.make;
    });
    this.getSegmentApplication();
  }

  async getSegmentApplication() {

    const values = {
      "oem_name": this.makeName,
      "segment": this.segmentName,
      "sub_segment": this.subSegmentName

    }
    this.config.loader('Loading...');
    this.config.postData('api/mobile/getSegmentApplication', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {
        this.config.loaderDismiss();
        this.isSegmentTable=true;
        this.segmentApplicationJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        

      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');

      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });

  }

  async gotoSegmentPartDetails(partDetails) {
    this.config.logDetails(this.makeName,this.segmentName,this.subSegmentName,partDetails.newpartno,partDetails.application)
    const values = {
      oldPartNo: partDetails.oldpartno,
      newPartNo: partDetails.newpartno,
      oemPartNo: partDetails.oempartno,
      engine: partDetails.engine,
      application: partDetails.application,
      makeName:this.makeName
    }
    let navigation: NavigationExtras = {
      queryParams: {
        segmentApplication: JSON.stringify(values)
      }
    };

    this.router.navigate(['/segmentpartdetails'], navigation)
  }

}
