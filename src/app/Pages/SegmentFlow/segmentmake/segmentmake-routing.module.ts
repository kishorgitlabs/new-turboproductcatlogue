import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegmentmakePage } from './segmentmake.page';

const routes: Routes = [
  {
    path: '',
    component: SegmentmakePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SegmentmakePageRoutingModule {}
