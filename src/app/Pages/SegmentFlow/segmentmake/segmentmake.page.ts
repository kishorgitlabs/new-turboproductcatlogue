import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-segmentmake',
  templateUrl: './segmentmake.page.html',
  styleUrls: ['./segmentmake.page.scss'],
})
export class SegmentmakePage implements OnInit {

  ngSearchTerm;
  segmentMakeJson=[];    

  segmentName:string;
  subSegment:string;
  constructor(
    private activateroute:ActivatedRoute,
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {

    this.activateroute.queryParams.subscribe((res)=>{
      const obj =JSON.parse(res.subSegmentObj);
      this.segmentName=obj.segmentName;
      this.subSegment=obj.subSegment;
    });
    

this.getSegmentMakeDetails();
  }

  async getSegmentMakeDetails(){

    const values= {
      "segment":this.segmentName,
      "sub_segment":this.subSegment
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getSegmentManufacturer',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.segmentMakeJson=response.data;
     

      }
      else if(response.result === false){
       this.config.loaderDismiss();
       
      
     }
      else{
       this.config.loaderDismiss();
       this.config.alertService('Something went wrong . Please try again later .');
       
      }
   },err=>{
    this.config.loaderDismiss();
    this.config.alertService('Something went wrong . Please try again later .');;
   });

 }


 async gotoSegmentApplication(make){
  this.config.logDetails(make,this.segmentName,this.subSegment,'','')
  const values={
    make:make,
    segment:this.segmentName,
    subsegment:this.subSegment
  }

  let navigation:NavigationExtras={
    queryParams:{
      segmentMakeObj:JSON.stringify(values)
    }
  };
this.router.navigate(['/segmentapplication'],navigation)
  

 }

}
