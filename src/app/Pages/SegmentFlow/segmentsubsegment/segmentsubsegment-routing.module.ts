import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegmentsubsegmentPage } from './segmentsubsegment.page';

const routes: Routes = [
  {
    path: '',
    component: SegmentsubsegmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SegmentsubsegmentPageRoutingModule {}
