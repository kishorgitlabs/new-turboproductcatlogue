import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SegmentsubsegmentPageRoutingModule } from './segmentsubsegment-routing.module';

import { SegmentsubsegmentPage } from './segmentsubsegment.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SegmentsubsegmentPageRoutingModule,
    ComponentModule
  ],
  declarations: [SegmentsubsegmentPage]
})
export class SegmentsubsegmentPageModule {}
