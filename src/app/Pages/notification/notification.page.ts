import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  getNotificationJson:any=[];
  constructor(
    private config:ConfigService,
  ) { }

  ngOnInit() {
    this.getNotificationList();
  }

  async getNotificationList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/notificationdetails').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.getNotificationJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
         
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}

}
