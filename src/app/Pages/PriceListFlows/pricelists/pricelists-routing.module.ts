import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PricelistsPage } from './pricelists.page';

const routes: Routes = [
  {
    path: '',
    component: PricelistsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PricelistsPageRoutingModule {}
