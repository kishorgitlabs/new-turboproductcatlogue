import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentModule } from 'src/app/Component/Component.module';

import { IonicModule } from '@ionic/angular';

import { PricelistsPageRoutingModule } from './pricelists-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { PricelistsPage } from './pricelists.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PricelistsPageRoutingModule,
    NgSelectModule,
    ComponentModule,
    ReactiveFormsModule
  ],
  declarations: [PricelistsPage]
})
export class PricelistsPageModule {}
