import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-pricelists',
  templateUrl: './pricelists.page.html',
  styleUrls: ['./pricelists.page.scss'],
})
export class PricelistsPage implements OnInit {

  getSearchNoJson = [];
  segmentJson = [];
  ngModel: any;
  getSearchNo: any;
  segmentName: any;
  partno: any;
  getPriceListJson: any = [];
  isPriceListTable: boolean = false;
  getUserType: string;
  UserDetail: any;
  isSalesPrice: boolean = true;

  constructor(
    private config: ConfigService,
    private router: Router
  ) { }

  ngOnInit() {

    this.UserDetail = JSON.parse(localStorage.getItem('UserDetail'));

    this.getUserType = this.UserDetail.usertype;
    if (this.getUserType === 'End User') {
      this.isSalesPrice = false;
    }
  }


  async searchPartNo() {
    if (this.getSearchNo === '' || this.getSearchNo === undefined || this.getSearchNo === null) {
      this.config.toastfn('Enter any partno');
      return;
    }
    this.getPriceDetails();
  }


  async getPriceDetails() {
    const values = {
      "tca_newpartno": this.getSearchNo,
      "usertype": this.getUserType
    };

    console.log(this.getSearchNo);


    this.config.loader('Loading...');
    this.config.postData('api/mobile/getpricelist', values).subscribe((res) => {
      const response: any = res;
      if (response.result === true) {
        this.config.loaderDismiss();

        this.isPriceListTable = true;
        this.getPriceListJson = response.data;

      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        this.config.alertService('No Details Found .');
      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });
  }

  // async gotopartDetails(a){


  //   const values={
  //     oldPartNo:a.oldpartno,
  //     oempartno:a.oempartno,
  //     engine:a.engine,
  //     application:a.application,
  //      oemname:a.oem_name,
  //     newPartNo:a.newpartno,
  //     oempartnumber:a.oem_partnumber
  //   }

  //   let navigation:NavigationExtras={
  //     queryParams:{
  //       apllicationObj:JSON.stringify(values)
  //     }
  //   };
  // this.router.navigate([`/pricelistpartdetails`],navigation);

  // }

}
