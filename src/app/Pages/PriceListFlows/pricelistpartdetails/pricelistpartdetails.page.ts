import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/Services/config/config.service';
import { ModalController } from '@ionic/angular';
import { ImagemodalcomponentComponent } from 'src/app/Component/imagemodalcomponent/imagemodalcomponent.component';

@Component({
  selector: 'app-pricelistpartdetails',
  templateUrl: './pricelistpartdetails.page.html',
  styleUrls: ['./pricelistpartdetails.page.scss'],
})
export class PricelistpartdetailsPage implements OnInit {

    
  applicationPartDetailsJson =[];
  getOldPartNo:string;
  getOemPartNo:string;
  getEngineName:string;
  getApplication:string;
  getMakeName:string;
  getNewPartNo:string;
  type: string = 'Specification';
  userDetail:any;
   getUserType:string;
   getMobleNo:string;

  constructor(
    private modalCtrl: ModalController,
    private activateroute:ActivatedRoute,
    private config:ConfigService
  ) { }

  ngOnInit() {
    this.userDetail=JSON.parse(localStorage.getItem('UserDetail'));
  
     this.getMobleNo=this.userDetail.mobileno;
     this.getUserType=this.userDetail.usertype;

    this.activateroute.queryParams.subscribe((res)=>{

      const obj =JSON.parse(res.apllicationObj);
      this.getOldPartNo=obj.oldPartNo;
      this.getOemPartNo=obj.oempartnumber;
      this.getEngineName=obj.engine;
      this.getApplication=obj.application;
      this.getMakeName=obj.oem_name;
       this.getNewPartNo=obj.newPartNo;
    });
    this.getMakeApplicationPartDetails();
  }


  async openPreview(a) {
    const imgURL = a;
    const modal = await this.modalCtrl.create({
      component: ImagemodalcomponentComponent,
      cssClass: 'transparent-modal',
      componentProps: {
        img: imgURL
      }
    });
    modal.present();
  }

  
  async getMakeApplicationPartDetails(){

    const values= {
               "tca_newpartno":this.getNewPartNo,
                "mobileno":this.getMobleNo,
                "usertype":this.getUserType
    }
    
   this.config.loader('Loading...');
   this.config.postData('api/mobile/searchbypartno',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
     
        this.config.loaderDismiss(); 
     
       this.applicationPartDetailsJson=response.data;
      }
      else if(response.result === false){
        this.config.loaderDismiss();
        this.config.alertService('No Details Found .')

      }
      else{
       this.config.loaderDismiss();
       this.config.alertService('Something went wrong .Please try again later .');
     
      }
   },err=>{
    this.config.loaderDismiss();
       this.config.alertService('Something went wrong .Please try again later .');
     
   });

 }


}
