import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PricelistpartdetailsPageRoutingModule } from './pricelistpartdetails-routing.module';

import { PricelistpartdetailsPage } from './pricelistpartdetails.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    PricelistpartdetailsPageRoutingModule
  ],
  declarations: [PricelistpartdetailsPage]
})
export class PricelistpartdetailsPageModule {}
