import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PricelistpartdetailsPage } from './pricelistpartdetails.page';

const routes: Routes = [
  {
    path: '',
    component: PricelistpartdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PricelistpartdetailsPageRoutingModule {}
