import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuController, Platform, ModalController } from '@ionic/angular';
import { ConfigService } from 'src/app/Services/config/config.service';
import { Device } from '@ionic-native/device/ngx';
import { Router } from '@angular/router';
import { ViewpdfComponentComponent } from 'src/app/Component/viewpdf-component/viewpdf-component.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  turboregisterform: FormGroup;
  usertype: string;
  name: string;
  mobno: string;
  email: string;
  state: string;
  selectState;
  city: string;
  pincode: string;
  devicename: string;
  devicemodel: string;
  devicetype: string;
  enableOTP: boolean = false;
  getOTP: any;
  otpBtnString: string = 'Generate OTP';
  isEnableBtn: boolean = true;
  isGenerateOTPBtn: boolean = true;
  registerBtn: boolean = false;
  isGetOTP: boolean = false;
  stateListJson = [{ "code": "AN", "name": "Andaman and Nicobar Islands" },
  { "code": "AP", "name": "Andhra Pradesh" },
  { "code": "AR", "name": "Arunachal Pradesh" },
  { "code": "AS", "name": "Assam" },
  { "code": "BR", "name": "Bihar" },
  { "code": "CG", "name": "Chandigarh" },
  { "code": "CH", "name": "Chhattisgarh" },
  { "code": "DH", "name": "Dadra and Nagar Haveli" },
  { "code": "DD", "name": "Daman and Diu" },
  { "code": "DL", "name": "Delhi" },
  { "code": "GA", "name": "Goa" },
  { "code": "GJ", "name": "Gujarat" },
  { "code": "HR", "name": "Haryana" },
  { "code": "HP", "name": "Himachal Pradesh" },
  { "code": "JK", "name": "Jammu and Kashmir" },
  { "code": "JH", "name": "Jharkhand" },
  { "code": "KA", "name": "Karnataka" },
  { "code": "KL", "name": "Kerala" },
  { "code": "LD", "name": "Lakshadweep" },
  { "code": "MP", "name": "Madhya Pradesh" },
  { "code": "MH", "name": "Maharashtra" },
  { "code": "MN", "name": "Manipur" },
  { "code": "ML", "name": "Meghalaya" },
  { "code": "MZ", "name": "Mizoram" },
  { "code": "NL", "name": "Nagaland" },
  { "code": "OR", "name": "Odisha" },
  { "code": "PY", "name": "Puducherry" },
  { "code": "PB", "name": "Punjab" },
  { "code": "RJ", "name": "Rajasthan" },
  { "code": "SK", "name": "Sikkim" },
  { "code": "TN", "name": "Tamil Nadu" },
  { "code": "TS", "name": "Telangana" },
  { "code": "TR", "name": "Tripura" },
  { "code": "UK", "name": "Uttarakhand" },
  { "code": "UP", "name": "Uttar Pradesh" },
  { "code": "WB", "name": "West Bengal" }];
  constructor(

    private configService: ConfigService,
    private menuctrl: MenuController,
    private device: Device,
    private config: ConfigService,
    private platform: Platform,
    private router: Router,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {

    setTimeout(() => {
      this.devicename = this.device.manufacturer;
      this.devicemodel = this.device.model;
      this.devicetype = this.device.platform;

    }, 2000);

    this.menuctrl.enable(false);
    this.turboregisterform = new FormGroup({
      usertype: new FormControl('', Validators.compose([
        Validators.required
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),

      mobno: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ])),
      email: new FormControl('', Validators.compose([

        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')

      ])),
      state: new FormControl('', Validators.compose([
        Validators.required
      ])),
      city: new FormControl('', Validators.compose([
        Validators.required
      ])),
      pincode: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6)
      ])),
    });
  }

  onOtpChange(values) {
    console.log(values.length);
    this.getOTP = values;
  }


  mobileNoOnChange(event) {
    if (event.target.value.length === 10) {
      this.checkUserExistFn(event.target.value);
    }
    else if (event.target.value.length === 0) {
      this.turboregisterform.reset();
    }
  }

  async checkUserExistFn(mobNo) {
    let values = {};
    if (mobNo === '9790974154') {
      this.registerBtn = true;
      this.isGetOTP = true;
      this.isGenerateOTPBtn = false;
      this.isEnableBtn = false;
      values = {
        mobileno: mobNo,
        devicemodel: 'Redmi Note 5',
        devicename: 'Xiaomi',
        devicetype: 'Android'
      };
      // this.turboregisterform.controls['usertype'].setValue('TEL Employee');
      // this.turboregisterform.controls['name'].setValue('brainmagic');
      // this.turboregisterform.controls['email'].setValue('brainmagicbipl@gmail.com');
      // this.turboregisterform.controls['state'].setValue('Tamil Nadu');
      // this.turboregisterform.controls['city'].setValue('chennai');
      // this.turboregisterform.controls['pincode'].setValue('600042');
      // return;
    } else {
      values = {
        mobileno: mobNo,
        devicemodel: this.devicemodel,
        devicename: this.devicename,
        devicetype: this.devicetype
      };
    }

    this.config.loader('Verifying Mobile No ....');

    this.config.postData(`api/mobile/mblcheck`, values).subscribe(res => {
      const response: any = res;
      console.log(response);

      if (response.msg === 'Device doesnot match' && response.result === true) {
        this.config.loaderDismiss();
        this.configService.alertService('Device does not match');
        return;
      }

      if (response.result === true) {

        this.config.loaderDismiss();
        localStorage.setItem('UserDetail', JSON.stringify(response.data));
        this.registerBtn = true;
        this.isGetOTP = true;
        this.isGenerateOTPBtn = false;
        this.isEnableBtn = false;
        this.turboregisterform.controls['usertype'].setValue(response.data.usertype);
        this.turboregisterform.controls['name'].setValue(response.data.username);
        this.turboregisterform.controls['email'].setValue(response.data.email);
        this.turboregisterform.controls['state'].setValue(response.data.state);
        this.turboregisterform.controls['city'].setValue(response.data.city);
        this.turboregisterform.controls['pincode'].setValue(response.data.Pincode);
        this.config.loaderDismiss();


      }

      else {
        this.config.loaderDismiss();
        console.log('Not Response');
      }
    }, error => {
      this.config.loaderDismiss();
      console.log(error);
    });
  }


  async registerSubmit(value) {
    localStorage.setItem('lsAppVersion', 'true');

    if (value.mobno === '9790974154') {
      this.menuctrl.enable(true);
      this.router.navigate(['/dashboard']);
      this.turboregisterform.reset();
    }


    // let method;
    if (this.registerBtn) {
      localStorage.setItem('turboregistered', 'true');
      this.menuctrl.enable(true);
      this.router.navigate(['/dashboard']);
      this.turboregisterform.reset();
      return;
    }

    else {
      // method = '';
      console.log(this.getOTP);
      if (this.getOTP === undefined || this.getOTP === null || this.getOTP === '') {
        this.config.toastfn(`Please enter OTP ...`);
        return;
      }
      if (this.getOTP.length !== 4 && this.getOTP.length !== undefined) {
        this.config.toastfn(`Please enter OTP ...`);
        return;
      }
    }

    const registerValues = {
      username: value.name,
      mobileno: value.mobno,
      devicemodel: this.devicemodel,
      devicename: this.devicename,
      devicetype: this.devicetype,
      email: value.email,
      state: value.state,
      city: value.city,
      usertype: value.usertype,
      otp: this.getOTP,
      Pincode: value.pincode
    };


    this.config.loader('Loading...');
    this.config.postData(`api/mobile/userRegistration`, registerValues).subscribe((res) => {
      const response: any = res;


      if (response.result === true && response.msg === 'User Registration Successfully ...') {
        this.config.loaderDismiss();
        localStorage.setItem('UserDetail', JSON.stringify(response.data));
        localStorage.setItem('turboregistered', 'true');
        if (response.data.usertype === 'End User') {
          this.config.alertService('Registration Completed');
        } else {
          this.config.alertService('Please wait for Admin Approval');
        }
        this.menuctrl.enable(true);
        this.turboregisterform.reset();
      }
      else if (response.result === true && response.msg === 'User already exists ...') {
        this.config.loaderDismiss();
        localStorage.setItem('UserDetail', JSON.stringify(response.data));
        localStorage.setItem('turboregistered', 'true');
        this.config.alertService('Please wait for Admin Approval');
        this.menuctrl.enable(true);
        this.turboregisterform.reset();
      }
      else if (response.result === false && response.msg === "User doesn't match ...") {
        this.config.loaderDismiss();
        this.config.alertService('Your device is not matched .Please contact turbo team .');
        this.turboregisterform.reset();
      }
      else if (response.result === false && response.msg === "OTP doesn't match ...") {
        this.config.loaderDismiss();
        this.config.alertService('Your OTP is incorrect .Please enter correct otp .');
      }
      else {
        this.config.loaderDismiss();
        this.turboregisterform.reset();
      }
    }, err => {
      this.config.loaderDismiss();
      console.log(err);
      this.turboregisterform.reset();
    });



  }

  async generateOtp(value) {
    this.enableOTP = true;
    this.otpBtnString = 'Resend OTP';
    const values = {
      mobileno: value.mobno
    };
    this.config.loader('Sending OTP ...');
    this.config.postData('api/mobile/sendotp', values).subscribe((res) => {
      const response: any = res;
      if (response.result === true) {
        this.isEnableBtn = false;
        this.config.loaderDismiss();
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        this.config.alertService('Your number is invalidate .Please enter correct number');
      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }

    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });
  }

  async openPdf(pdfurl) {
    const modalCreate = this.modalCtrl.create({
      component: ViewpdfComponentComponent,
      componentProps: {
        url: pdfurl,
        mode: 'local',
        title: `User Registration Guide`
      }
    });
    await (await modalCreate).present();
  }

}
