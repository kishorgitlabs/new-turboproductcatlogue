import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { MatExpansionModule } from '@angular/material/expansion';
import { MakepartdetailsPageRoutingModule } from './makepartdetails-routing.module';
import { MakepartdetailsPage } from './makepartdetails.page';
import { ComponentModule } from 'src/app/Component/Component.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MakepartdetailsPageRoutingModule,
    ComponentModule,
    MatExpansionModule
  ],
  declarations: [MakepartdetailsPage]
})
export class MakepartdetailsPageModule {}
