import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MakepartdetailsPage } from './makepartdetails.page';

const routes: Routes = [
  {
    path: '',
    component: MakepartdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MakepartdetailsPageRoutingModule {}
