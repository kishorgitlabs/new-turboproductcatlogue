import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MakesubsegmentPageRoutingModule } from './makesubsegment-routing.module';

import { MakesubsegmentPage } from './makesubsegment.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MakesubsegmentPageRoutingModule,
    ComponentModule
  ],
  declarations: [MakesubsegmentPage]
})
export class MakesubsegmentPageModule {}
