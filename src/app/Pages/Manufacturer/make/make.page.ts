import { ConfigService } from 'src/app/Services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-make',
  templateUrl: './make.page.html',
  styleUrls: ['./make.page.scss'],
})
export class MakePage implements OnInit {

  ngSearchTerm;
  makeListJson:any =[];
 
  make:string;
  segment:string;
  SubSegment:string;
  Partno:string;
  VehicleModel:string;


  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getMakeList();

  }
  async getMakeList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/getMake').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.makeListJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}
async gotoMakeSegment(obj){

  this.config.logDetails(obj.make,'','','','')
 
const values={
  make:obj
}

const navigation:NavigationExtras={
  queryParams:{
    makeObj:JSON.stringify(values)
  }
};
 this.router.navigate(['/makesegment'],navigation)
}
}
