import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MakePageRoutingModule } from './make-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { MakePage } from './make.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MakePageRoutingModule,
    ComponentModule,
    Ng2SearchPipeModule
  ],
  declarations: [MakePage]
})
export class MakePageModule {}
