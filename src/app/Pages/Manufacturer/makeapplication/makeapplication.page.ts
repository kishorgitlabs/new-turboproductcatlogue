import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';
@Component({
  selector: 'app-makeapplication',
  templateUrl: './makeapplication.page.html',
  styleUrls: ['./makeapplication.page.scss'],
})
export class MakeapplicationPage implements OnInit {

  getMakeName: string;
  getSegmentName: string;
  getSubSegmentName: string;
  makeApplicationtJson = [];
  isMakeTable: boolean = false;
  constructor(
    private activateroute: ActivatedRoute,
    private config: ConfigService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res) => {
      const obj = JSON.parse(res.makeSubSegmentObj);
      this.getMakeName = obj.makeName;
      this.getSegmentName = obj.segmentName;
      this.getSubSegmentName = obj.subSegment;
    });
    this.getMakeApplication();
  }

  async getMakeApplication() {
    const values = {
      oem_name: this.getMakeName,
      segment: this.getSegmentName,
      sub_segment: this.getSubSegmentName
    };

    this.config.loader('Loading...');
    this.config.postData('api/mobile/getMakeApplication', values).subscribe((res) => {
      const response: any = res;
      if (response.result === true) {
        this.config.loaderDismiss();
        this.isMakeTable = true;
        this.makeApplicationtJson = response.data;
        console.log(this.makeApplicationtJson);
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });
  }

  async gotoPartDetails(a) {
    this.config.logDetails(this.getMakeName, this.getSegmentName, this.getSubSegmentName, a.newpartno, a.application);
    const values = {
      oldPartNo: a.oldpartno,
      oempartno: a.oempartno,
      engine: a.engine,
      application: a.application,
      makename: this.getMakeName,
      newPartNo: a.newpartno
    };
    const navigation: NavigationExtras = {
      queryParams: {
        apllicationObj: JSON.stringify(values)
      }
    };
    this.router.navigate([`/makepartdetails`], navigation);
  }

}
