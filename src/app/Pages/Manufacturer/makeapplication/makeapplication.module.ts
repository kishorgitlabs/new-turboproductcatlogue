import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MakeapplicationPageRoutingModule } from './makeapplication-routing.module';

import { MakeapplicationPage } from './makeapplication.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MakeapplicationPageRoutingModule,
    ComponentModule
  ],
  declarations: [MakeapplicationPage]
})
export class MakeapplicationPageModule {}
