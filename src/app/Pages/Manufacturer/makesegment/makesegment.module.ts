import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MakesegmentPageRoutingModule } from './makesegment-routing.module';

import { MakesegmentPage } from './makesegment.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MakesegmentPageRoutingModule,
    ComponentModule
  ],
  declarations: [MakesegmentPage]
})
export class MakesegmentPageModule {}
