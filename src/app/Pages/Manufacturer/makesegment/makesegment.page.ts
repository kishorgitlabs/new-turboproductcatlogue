import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-makesegment',
  templateUrl: './makesegment.page.html',
  styleUrls: ['./makesegment.page.scss'],
})
export class MakesegmentPage implements OnInit {

  getMakeName:string;
  segmentListJson=[];

  constructor(
    private activateroute:ActivatedRoute,
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res)=>{
      const obj=JSON.parse(res.makeObj);
      this.getMakeName=obj.make;
    });
    this.getSegmentDetails();
  }

  async getSegmentDetails(){

    const values= {
     "oem_name":this.getMakeName
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getMakeSegment',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.segmentListJson=response.data;

      }
      else if(response.result === false){
       this.config.loaderDismiss();
      
     }
      else{
       this.config.loaderDismiss();
       this.config.alertService('Something went wrong . Please try again later .');
      }
   },err=>{
    this.config.loaderDismiss();
    this.config.alertService('Something went wrong . Please try again later .');
   });

 }


 async gotoMakeSubSegment(segment){

  this.config.logDetails(this.getMakeName,segment,'','','')
  const values={
    makename:this.getMakeName,
    segmentName:segment
  }

  let navigation:NavigationExtras={
    queryParams:{
      makeSegmentObj:JSON.stringify(values)
    }
  };

  this.router.navigate([`/makesubsegment`],navigation)
}

}
