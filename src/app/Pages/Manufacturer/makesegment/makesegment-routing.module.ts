import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MakesegmentPage } from './makesegment.page';

const routes: Routes = [
  {
    path: '',
    component: MakesegmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MakesegmentPageRoutingModule {}
