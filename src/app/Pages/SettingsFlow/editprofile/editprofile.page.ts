import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditprofilePage implements OnInit {

  editProfileForm:FormGroup
  UserDetail: any;
  mobileno:string;
  usertype:string;
  username:string;
  mail:string;
  state:string;
  city:string;

  constructor(
    private config: ConfigService,
  ) { }

  ngOnInit() {

    this.UserDetail = JSON.parse(localStorage.getItem('UserDetail'));

    this.usertype=this.UserDetail.usertype;
    this.mobileno=this.UserDetail.mobileno;


    this.editProfileForm=new FormGroup({

      name:new FormControl('',Validators.compose([
        Validators.required
      ])),

      email:new FormControl('',Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
     
      state:new FormControl('',Validators.compose([
        Validators.required
      ])),

      city:new FormControl('',Validators.compose([
        Validators.required
      ])),


    });

    this.editProfileForm.controls['name'].setValue(this.UserDetail.username);
    this.editProfileForm.controls['email'].setValue(this.UserDetail.email);
    this.editProfileForm.controls['state'].setValue(this.UserDetail.state);
    this.editProfileForm.controls['city'].setValue(this.UserDetail.city);
  }

   
  async update(formvalue){


    const values = {
      "mobileno": this.mobileno,
      "username": formvalue.name,
      "email": formvalue.email,
      "state":formvalue.state,
      "city":formvalue.city

    }

   
    this.config.loader('Loading...');
    this.config.postData('api/mobile/editregister', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {
        this.config.loaderDismiss();
        this.editProfileForm.reset();
         this.config.alertService('Profile Updated Successfully .')
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        

      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');

      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });

  }

}
