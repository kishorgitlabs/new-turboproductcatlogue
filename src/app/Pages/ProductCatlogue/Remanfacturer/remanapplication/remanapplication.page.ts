import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-remanapplication',
  templateUrl: './remanapplication.page.html',
  styleUrls: ['./remanapplication.page.scss'],
})
export class RemanapplicationPage implements OnInit {

  makeApplicationtJson=[];
  getMake:string;
  getSegment:string;
  getSubSegment:string;
  userDetail:any;
   getUserType:string;
   getMobleNo:string;

   isOem:boolean=true;
   isModel:boolean=true;

  constructor(
    private config:ConfigService,
    private router:Router,
    private activateroute:ActivatedRoute
  ) { }

  ngOnInit() {

    
  this.userDetail=JSON.parse(localStorage.getItem('UserDetail'));
  
     this.getMobleNo=this.userDetail.mobileno;
     this.getUserType=this.userDetail.usertype;

    this.activateroute.queryParams.subscribe((res)=>{
      const obj= JSON.parse(res.applicationObj);
      this.getMake=obj.make;
      this.getSegment=obj.segment;
        this.getSubSegment=obj.subsegment;
    });

    if (this.getUserType === 'End User' || this.getUserType === 'Distributor') {
      
      this.isModel=false;
      this.isOem=false;
    }
this.getApplication();
  }

  async getApplication(){

   
    const values={
        "oem_name":this.getMake,
        "segment":this.getSegment,
         "sub_segment":this.getSubSegment,
         "mobileno":this.getMobleNo,
         "usertype":this.getUserType
       
    }
  
    this.config.loader('Loading...');
     this.config.postData('api/mobile/remanapplication',values).subscribe((res)=>{
    
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
        
        this.makeApplicationtJson=response.data;
     console.log(this.makeApplicationtJson);
       }
        else if(response.result === false){
         this.config.loaderDismiss();
         
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}

}
