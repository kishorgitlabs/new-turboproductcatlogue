import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RemanapplicationPage } from './remanapplication.page';

const routes: Routes = [
  {
    path: '',
    component: RemanapplicationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemanapplicationPageRoutingModule {}
