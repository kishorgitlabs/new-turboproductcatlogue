import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RemanapplicationPageRoutingModule } from './remanapplication-routing.module';
import { ComponentModule } from 'src/app/Component/Component.module';

import { RemanapplicationPage } from './remanapplication.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RemanapplicationPageRoutingModule,
    ComponentModule
  ],
  declarations: [RemanapplicationPage]
})
export class RemanapplicationPageModule {}
