import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RemansegmentPage } from './remansegment.page';

const routes: Routes = [
  {
    path: '',
    component: RemansegmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemansegmentPageRoutingModule {}
