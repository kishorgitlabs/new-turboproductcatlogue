import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-remansegment',
  templateUrl: './remansegment.page.html',
  styleUrls: ['./remansegment.page.scss'],
})
export class RemansegmentPage implements OnInit {

  makeListJson: any = [];
  getMake: string;
  isMakeTable: boolean = false;
  isDesc: boolean = true;
  getUserType: string;
  userDetail: any;
  isSalesPrice: boolean = true;
  constructor(
    private config: ConfigService,
    private router: Router,
    private activateroute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('UserDetail'));
    this.getUserType = this.userDetail.usertype;
    if (this.getUserType === 'End User') {
      this.isSalesPrice = false;
    }
    this.activateroute.queryParams.subscribe((res) => {
      const obj = JSON.parse(res.makeObj);
      this.getMake = obj.make;
    });
    this.getSegmentList();
    this.getSegmentListPrice();
  }

  async getSegmentListPrice() {

    const values = {
      'oem_name': this.getMake
    };

    this.config.loader('Loading...');
    this.config.postData('api/mobile/getRemanoemlistprice', values).subscribe((res) => {
      console.log('Subscription Success');
      const response: any = res;
      if (response.result === true) {
        this.isMakeTable = true;
        console.log('Respnse Success');
        this.config.loaderDismiss();
        this.makeListJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();

      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });

  }

  async getSegmentList() {

    const values = {
      'oem_name': this.getMake
    };

    this.config.loader('Loading...');
    this.config.postData('api/mobile/getRemanoem', values).subscribe((res) => {
      console.log('Subscription Success');
      const response: any = res;
      if (response.result === true) {
        this.isMakeTable = true;
        console.log('Respnse Success');
        this.config.loaderDismiss();
        this.makeListJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();

      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });

  }

  // async gotoremanSubSegment(a){
  //   this.config.logDetails(this.getMake,a.segment,'','','')
  //   const values ={
  //     make:this.getMake,
  //     segment:a.segment
  //   }

  //   const navigation:NavigationExtras={

  //     queryParams:{
  //       segmentObj:JSON.stringify(values)
  //     }
  //   };
  //   this.router.navigate(['/remansubsegment'],navigation)
  // }

}


