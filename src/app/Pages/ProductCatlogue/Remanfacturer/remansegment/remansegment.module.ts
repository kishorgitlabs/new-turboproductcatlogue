import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RemansegmentPageRoutingModule } from './remansegment-routing.module';
import { ComponentModule } from 'src/app/Component/Component.module';

import { RemansegmentPage } from './remansegment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RemansegmentPageRoutingModule,
    ComponentModule
  ],
  declarations: [RemansegmentPage]
})
export class RemansegmentPageModule {}
