import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-remansubsegment',
  templateUrl: './remansubsegment.page.html',
  styleUrls: ['./remansubsegment.page.scss'],
})
export class RemansubsegmentPage implements OnInit {

  makeListJson:any =[];
  getMake:string;
  getSegment:string;

  constructor(
    private config:ConfigService,
    private router:Router,
    private activateroute:ActivatedRoute
  ) { }

  ngOnInit() {

    this.activateroute.queryParams.subscribe((res)=>{
      const obj= JSON.parse(res.segmentObj);
      this.getMake=obj.make;
      this.getSegment=obj.segment;

    })
    this.getSubSegmentList();
  }

  async getSubSegmentList(){

    const values={
        "oem_name":this.getMake,
        "segment":this.getSegment
    }
    
    this.config.loader('Loading...');
     this.config.postData('api/mobile/remansubsegment',values).subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
         
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.makeListJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
       
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}
async gotoApplication(a){
  this.config.logDetails(this.getMake,this.getSegment,a.subsegment,'','')
  const values ={
    make:this.getMake,
    segment:this.getSegment,
    subsegment:a.subsegment
  }

  const navigation:NavigationExtras={
   
    queryParams:{
      applicationObj:JSON.stringify(values)
    }
  };
  this.router.navigate(['/remanapplication'],navigation)
}

}
