import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RemansubsegmentPage } from './remansubsegment.page';

const routes: Routes = [
  {
    path: '',
    component: RemansubsegmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemansubsegmentPageRoutingModule {}
