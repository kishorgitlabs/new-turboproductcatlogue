import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RemansubsegmentPageRoutingModule } from './remansubsegment-routing.module';
import { ComponentModule } from 'src/app/Component/Component.module';

import { RemansubsegmentPage } from './remansubsegment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RemansubsegmentPageRoutingModule,
    ComponentModule
  ],
  declarations: [RemansubsegmentPage]
})
export class RemansubsegmentPageModule {}
