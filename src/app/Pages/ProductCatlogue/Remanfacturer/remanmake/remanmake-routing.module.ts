import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RemanmakePage } from './remanmake.page';

const routes: Routes = [
  {
    path: '',
    component: RemanmakePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemanmakePageRoutingModule {}
