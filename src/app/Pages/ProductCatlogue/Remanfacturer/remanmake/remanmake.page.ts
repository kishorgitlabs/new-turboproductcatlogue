import { NavigationExtras, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-remanmake',
  templateUrl: './remanmake.page.html',
  styleUrls: ['./remanmake.page.scss'],
})
export class RemanmakePage implements OnInit {

  ngSearchTerm;
  makeListJson:any =[];
  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getMakeList();
  }

  async getMakeList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/remanmake').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
         
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.makeListJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
       
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}

async gotoRemanSegment(a){
  
  this.config.logDetails(a.make,'','','','')
  const values={
    make:a.make
  }
 
  let navigation:NavigationExtras={
    queryParams:{
      makeObj:JSON.stringify(values)
    }
  };

  this.router.navigate(['/remansegment'],navigation);

}



}
