import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RemanmakePageRoutingModule } from './remanmake-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { RemanmakePage } from './remanmake.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RemanmakePageRoutingModule,
    ComponentModule,
    Ng2SearchPipeModule
  ],
  declarations: [RemanmakePage]
})
export class RemanmakePageModule {}
