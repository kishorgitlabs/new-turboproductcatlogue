import { JsonpClientBackend } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-allmakeoemname',
  templateUrl: './allmakeoemname.page.html',
  styleUrls: ['./allmakeoemname.page.scss'],
})
export class AllmakeoemnamePage implements OnInit {


  ngSearchTerm;
  makeListJson:any =[];
  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getMakeList();
  }

  async getMakeList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/getallmake').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
         
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.makeListJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
       
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}


async gotoApplication(a){

  const values={
    newpart:a.newpartno,
    oldpart:a.oldpartno,
    listprice:a.listprice,
    mrp:a.mrp,
    oemname:a.OEM_Name,
    description:a.description
  }

   const navigation:NavigationExtras={
     queryParams:{
       oemNameObj:JSON.stringify(values)
     }
   };
 
   this.router.navigate(['/allmakeapplication'],navigation)
}

}
