import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllmakeoemnamePageRoutingModule } from './allmakeoemname-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { AllmakeoemnamePage } from './allmakeoemname.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllmakeoemnamePageRoutingModule,
    ComponentModule,
    Ng2SearchPipeModule
  ],
  declarations: [AllmakeoemnamePage]
})
export class AllmakeoemnamePageModule {}
