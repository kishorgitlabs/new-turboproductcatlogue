import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllmakeapplicationPageRoutingModule } from './allmakeapplication-routing.module';

import { AllmakeapplicationPage } from './allmakeapplication.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllmakeapplicationPageRoutingModule,
    ComponentModule
  ],
  declarations: [AllmakeapplicationPage]
})
export class AllmakeapplicationPageModule {}
