import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllmakeapplicationPage } from './allmakeapplication.page';

const routes: Routes = [
  {
    path: '',
    component: AllmakeapplicationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllmakeapplicationPageRoutingModule {}
