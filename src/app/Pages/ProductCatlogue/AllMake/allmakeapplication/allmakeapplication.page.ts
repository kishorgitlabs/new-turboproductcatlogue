import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-allmakeapplication',
  templateUrl: './allmakeapplication.page.html',
  styleUrls: ['./allmakeapplication.page.scss'],
})
export class AllmakeapplicationPage implements OnInit {

  getoldpartno:string;
  getnewpartno:string;
  listprice:string;
  description:string;
  makeName:string;
oemListJson=[];
  isPricelist:boolean=true;
  isDesc:boolean=true;
  getUserType:string;
  userDetail:any;

  constructor(
    private activateroute:ActivatedRoute,
    private config: ConfigService
  ) { }

  ngOnInit() {

    this.userDetail=JSON.parse(localStorage.getItem('UserDetail'));

    this.getUserType=this.userDetail.usertype;

    this.activateroute.queryParams.subscribe((res)=>{
      const obj =JSON.parse(res.oemNameObj);
      this.getoldpartno=obj.oldpart;
      this.getnewpartno=obj.newpart;
      this.listprice=obj.listprice;
      this.description=obj.description;
      this.makeName=obj.oemname
    });

    
    if (this.getUserType === 'End User' ) {
      
      this.isDesc=false;
      this.isPricelist=false;
    }
    this.getAllOEMList();
  }
  async getAllOEMList() {

    const values = {
      "OEM_Name": this.makeName
    }
    this.config.loader('Loading...');
    this.config.postData('api/mobile/getallmakeoem', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {

        this.config.loaderDismiss();
this.isPricelist=true;
        this.oemListJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        this.config.alertService('No Details Found .')

      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong .Please try again later .');

      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong .Please try again later .');

    });

  }

}
