import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WhatsnewPageRoutingModule } from './whatsnew-routing.module';
import { ComponentModule } from 'src/app/Component/Component.module';

import { WhatsnewPage } from './whatsnew.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WhatsnewPageRoutingModule,
    ComponentModule
  ],
  declarations: [WhatsnewPage]
})
export class WhatsnewPageModule {}
