import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-whatsnew',
  templateUrl: './whatsnew.page.html',
  styleUrls: ['./whatsnew.page.scss'],
})
export class WhatsnewPage implements OnInit {

  makeListJson:any =[];
   isWhatsnew:boolean=false;

  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getWhatsNewList();
  }


  async getWhatsNewList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/whatsnew').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.isWhatsnew=true
         this.makeListJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
         
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}

async gotoWhatsnewPartDetails(a){

  const values={
    old:a.tca_oldpartno,
    new:a.tca_newpartno,
    oempartno:a.oem_partnumber,
    engine:a.engine,
    application:a.application
  }

  const navigation:NavigationExtras={
    queryParams:{
      whatsnewObj:JSON.stringify(values)
    }
  };
  this.router.navigate(['/whatsnewpartdetails'],navigation);
}

}
