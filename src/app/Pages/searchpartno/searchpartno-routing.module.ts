import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchpartnoPage } from './searchpartno.page';

const routes: Routes = [
  {
    path: '',
    component: SearchpartnoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchpartnoPageRoutingModule {}
