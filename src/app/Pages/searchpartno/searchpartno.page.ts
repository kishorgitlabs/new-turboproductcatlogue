import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-searchpartno',
  templateUrl: './searchpartno.page.html',
  styleUrls: ['./searchpartno.page.scss'],
})
export class SearchpartnoPage implements OnInit {

  UserDetail: any;
  mobileno: string;
  usertype: string;
  searchPartNoJson: any = [];
  getSearchNo: string;
  isSearchPartNotable: boolean = false;

  constructor(
    private router: Router,
    private config: ConfigService
  ) { }

  ngOnInit() {
    this.UserDetail = JSON.parse(localStorage.getItem('UserDetail'));
    this.mobileno = this.UserDetail.mobileno;
    this.usertype = this.UserDetail.usertype;
  }


  async searchPartNo() {


    if (this.getSearchNo === '' || this.getSearchNo === undefined || this.getSearchNo === null) {
      this.config.toastfn('Enter any Part No');
      return;
    }
    const values = {
      "tca_newpartno": this.getSearchNo,
      "mobileno": this.mobileno,
      "usertype": this.usertype
    }

    this.config.loader('Loading...');
    this.config.postData('api/mobile/searchbypartno', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {
        this.config.loaderDismiss();
        this.isSearchPartNotable = true;
        this.searchPartNoJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        this.isSearchPartNotable = false;
        this.searchPartNoJson = [];
        this.config.alertService('Partno doesnot match')
      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });

  }


  async gotoPartDetails(a) {

    const values = {
      "newpartno": a.tca_newpartno,
      "oempartnumber": a.oem_partnumber,
      "engine": a.engine,
      "application": a.application,
      "oemname": a.oem_name,
      "oldpartno": a.tca_oldpartno
    }
    let navigation: NavigationExtras = {
      queryParams: {
        searchPartNoObj: JSON.stringify(values)
      }
    };
    this.router.navigate(['/segmentpartnodetails'], navigation);

  }
}
