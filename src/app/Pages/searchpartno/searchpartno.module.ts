import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchpartnoPageRoutingModule } from './searchpartno-routing.module';

import { SearchpartnoPage } from './searchpartno.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchpartnoPageRoutingModule,
    ComponentModule
  ],
  declarations: [SearchpartnoPage]
})
export class SearchpartnoPageModule {}
