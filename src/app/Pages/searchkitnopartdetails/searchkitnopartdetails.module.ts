import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchkitnopartdetailsPageRoutingModule } from './searchkitnopartdetails-routing.module';

import { SearchkitnopartdetailsPage } from './searchkitnopartdetails.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchkitnopartdetailsPageRoutingModule,
    ComponentModule
  ],
  declarations: [SearchkitnopartdetailsPage]
})
export class SearchkitnopartdetailsPageModule {}
