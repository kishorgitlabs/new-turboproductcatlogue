import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchkitnopartdetailsPage } from './searchkitnopartdetails.page';

const routes: Routes = [
  {
    path: '',
    component: SearchkitnopartdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchkitnopartdetailsPageRoutingModule {}
