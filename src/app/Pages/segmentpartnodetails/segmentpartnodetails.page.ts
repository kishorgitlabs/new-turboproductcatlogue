import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ImagemodalcomponentComponent } from 'src/app/Component/imagemodalcomponent/imagemodalcomponent.component';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-segmentpartnodetails',
  templateUrl: './segmentpartnodetails.page.html',
  styleUrls: ['./segmentpartnodetails.page.scss'],
})
export class SegmentpartnodetailsPage implements OnInit {

  applicationPartDetailsJson = [];
  getOldPartNo: string;
  getOemPartNo: string;
  getEngineName: string;
  getApplication: string;
  getMakeName: string;
  getNewPartNo: string;
  type: string = 'Specification';
  userDetail: any;
  getUserType: string;
  getMobleNo: string;
  isServiceInformation: boolean = true;
  isKitDetails: boolean = true;
  isTel: boolean = false;
  constructor(
    private modalCtrl: ModalController,
    private activateroute: ActivatedRoute,
    private config: ConfigService
  ) { }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('UserDetail'));
    this.getMobleNo = this.userDetail.mobileno;
    this.getUserType = this.userDetail.usertype;
    this.activateroute.queryParams.subscribe((res) => {
      const obj = JSON.parse(res.searchPartNoObj);
      this.getOldPartNo = obj.oldpartno;
      this.getOemPartNo = obj.oempartnumber;
      this.getEngineName = obj.engine;
      this.getApplication = obj.application;
      this.getMakeName = obj.oemname;
      this.getNewPartNo = obj.newpartno;
    });

    if (this.getUserType === 'End User') {
      this.isKitDetails = false;
      this.isServiceInformation = false;
    }

    if (this.getUserType === 'TEL Employee' || this.getUserType === 'ASC') {
      this.isTel = true;
    }

    if (this.getUserType === 'Distributor') {

      this.isServiceInformation = false;
    }
    this.getMakeApplicationPartDetails();
  }

  async openPreview(a) {
    const imgURL = a;
    const modal = await this.modalCtrl.create({
      component: ImagemodalcomponentComponent,
      cssClass: 'transparent-modal',
      componentProps: {
        img: imgURL
      }
    });
    modal.present();
  }


  async getMakeApplicationPartDetails() {

    const values = {
      "tca_newpartno": this.getNewPartNo,
      "oem_partnumber": this.getOemPartNo,
      "engine": this.getEngineName,
      "application": this.getApplication,
      "mobileno": this.getMobleNo,
      "usertype": this.getUserType
    }
    this.config.loader('Loading...');
    this.config.postData('api/mobile/getMakeApplicationParDetails', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {

        this.config.loaderDismiss();

        this.applicationPartDetailsJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();


      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong .Please try again later .');

      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong .Please try again later .');

    });

  }

}
