import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SegmentpartnodetailsPageRoutingModule } from './segmentpartnodetails-routing.module';

import { SegmentpartnodetailsPage } from './segmentpartnodetails.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SegmentpartnodetailsPageRoutingModule,
    ComponentModule
  ],
  declarations: [SegmentpartnodetailsPage]
})
export class SegmentpartnodetailsPageModule {}
