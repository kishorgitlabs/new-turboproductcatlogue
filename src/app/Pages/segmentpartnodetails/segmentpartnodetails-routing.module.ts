import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegmentpartnodetailsPage } from './segmentpartnodetails.page';

const routes: Routes = [
  {
    path: '',
    component: SegmentpartnodetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SegmentpartnodetailsPageRoutingModule {}
