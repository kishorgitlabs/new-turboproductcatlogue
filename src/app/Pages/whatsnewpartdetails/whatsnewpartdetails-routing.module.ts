import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WhatsnewpartdetailsPage } from './whatsnewpartdetails.page';

const routes: Routes = [
  {
    path: '',
    component: WhatsnewpartdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WhatsnewpartdetailsPageRoutingModule {}
