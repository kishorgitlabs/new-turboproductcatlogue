import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ImagemodalcomponentComponent } from 'src/app/Component/imagemodalcomponent/imagemodalcomponent.component';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-whatsnewpartdetails',
  templateUrl: './whatsnewpartdetails.page.html',
  styleUrls: ['./whatsnewpartdetails.page.scss'],
})
export class WhatsnewpartdetailsPage implements OnInit {

  applicationPartDetailsJson = [];
  getOldPartNo: string;
  getOemPartNo: string;
  getEngineName: string;
  getApplication: string;
  getMakeName: string;
  getNewPartNo: string;
  partdetails: string;
  userDetail: any;
  type: string = 'Specification';
  getUserType: string;
  getMobleNo: string;


  isKitDetails:boolean=true;
  isServiceInfo:boolean=true;
  isTel:boolean=false;
  constructor(
    private modalCtrl: ModalController,
    private activateroute: ActivatedRoute,
    private config: ConfigService
  ) { }

  ngOnInit() {



    this.userDetail = JSON.parse(localStorage.getItem('UserDetail'));

    this.getMobleNo = this.userDetail.mobileno;
    this.getUserType = this.userDetail.usertype;

    this.activateroute.queryParams.subscribe((res) => {

      const obj = JSON.parse(res.whatsnewObj);
      this.getOldPartNo = obj.old;
      this.getOemPartNo = obj.oempartno;
      this.getEngineName = obj.engine;
      this.getApplication = obj.application;
      // this.getMakeName=obj.makename;
      this.getNewPartNo = obj.new;
    });
    this.getMakeApplicationPartDetails();

    
    if (this.getUserType === 'End User' ) {
      this.isKitDetails=false;
      this.isServiceInfo=false;
    }
    
    
    if(this.getUserType === 'TEL Employee' || this.getUserType === 'ASC'){
      this.isTel=true;
        }
    
    if (this.getUserType === 'Distributor') {
    
      this.isServiceInfo=false;
    }
  }

  async openPreview(a) {
    const imgURL = a;
    const modal = await this.modalCtrl.create({
      component: ImagemodalcomponentComponent,
      cssClass: 'transparent-modal',
      componentProps: {
        img: imgURL
      }
    });
    modal.present();
  }


  async getMakeApplicationPartDetails() {

    const values = {
      "tca_newpartno": this.getNewPartNo,
      "oem_partnumber": this.getOemPartNo,
      "engine": this.getEngineName,
      "application": this.getApplication,
      "mobileno": this.getMobleNo,
      "usertype": this.getUserType
    }
    this.config.loader('Loading...');
    this.config.postData('api/mobile/getMakeApplicationParDetails', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {
      
        this.config.loaderDismiss();

        this.applicationPartDetailsJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();

      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');

      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');

    });

  }

}
