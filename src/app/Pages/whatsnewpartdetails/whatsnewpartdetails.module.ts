import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WhatsnewpartdetailsPageRoutingModule } from './whatsnewpartdetails-routing.module';
import { ComponentModule } from 'src/app/Component/Component.module';
import { WhatsnewpartdetailsPage } from './whatsnewpartdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WhatsnewpartdetailsPageRoutingModule,
    ComponentModule
  ],
  declarations: [WhatsnewpartdetailsPage]
})
export class WhatsnewpartdetailsPageModule {}
