import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-network',
  templateUrl: './network.page.html',
  styleUrls: ['./network.page.scss'],
})
export class NetworkPage implements OnInit {

  type: string;
  stateListJson = [];
  states = [];
  getCityJson = [];
  getASCCityJson=[];

  stateModel: any;
  cityModel: any;
  getDistributorListJson = [];
  getASCListJson = [];
  isDistributorList: boolean = false;
  isAscList: boolean = false
  stateASCList=[];

  constructor(
    private config: ConfigService
  ) { }

  ngOnInit() {
    this.type = 'ASC';
    this.getStateList('Distributor');
    this.getStateList('ASC');
  }

  async getStateList(type) {

    const values = {
      "type": type
    };

    this.config.loader('Loading...');
    this.config.postData('api/mobile/statelist',values).subscribe((res) => {
      console.log("Subscription Success");
      const response: any = res;
      if (response.result === true) {
        console.log("Respnse Success");
        this.config.loaderDismiss();
        if(type === 'Distributor'){
          this.stateListJson = response.data;
        }
        else if(type === 'ASC'){
this.stateASCList=response.data;
        }
        
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });

  }



 

  async getCityList(obj) {
    
this.cityModel=null;
    const values = {
      "state": obj.state,
      "type": 'Distributor',
    }

    this.config.loader('Loading...');
    this.config.postData('api/mobile/citylist', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {
        this.config.loaderDismiss();

        this.getCityJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();

      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });

  }


  async getCityASCList(obj) {
    
    this.cityModel=null;
        const values = {
          "state": obj.state,
          "type": 'ASC',
        }
    
        this.config.loader('Loading...');
        this.config.postData('api/mobile/citylist', values).subscribe((res) => {
    
          const response: any = res;
    
          if (response.result === true) {
            this.config.loaderDismiss();
    
            this.getASCCityJson = response.data;
          }
          else if (response.result === false) {
            this.config.loaderDismiss();
    
          }
          else {
            this.config.loaderDismiss();
            this.config.alertService('Something went wrong . Please try again later .');
          }
        }, err => {
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        });
    
      }

  async getNetworkDetails(type) {

    if (this.stateModel === '' || this.stateModel === undefined || this.stateModel === null) {
      this.config.toastfn('Select any state');
      return;
    }

    const values = {
      "type": type,
      "state": this.stateModel,
      "city": this.cityModel
    };
    console.log(this.stateModel);
    console.log(this.cityModel);
    this.config.loader('Loading...');
    this.config.postData('api/mobile/network', values).subscribe((res) => {
      const response: any = res;
      console.clear();
      console.log(response);
      if (response.result === true) {        
        
        this.config.loaderDismiss();
        if (type === 'Distributor') {
          this.isDistributorList = true;
          this.getDistributorListJson = response.data;
        } else if (type === 'ASC') {
          
        this.config.loaderDismiss();
          this.isAscList = true;
          this.getASCListJson =  response.data;
        }
        
        this.config.loaderDismiss();
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });

  }

  


}
