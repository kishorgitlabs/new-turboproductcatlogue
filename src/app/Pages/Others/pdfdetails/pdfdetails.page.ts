import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ViewpdfComponentComponent } from 'src/app/Component/viewpdf-component/viewpdf-component.component';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-pdfdetails',
  templateUrl: './pdfdetails.page.html',
  styleUrls: ['./pdfdetails.page.scss'],
})
export class PdfdetailsPage implements OnInit {

  getPDFJson=[];
  pdfName:string;

  constructor(
    private config:ConfigService,
    private router:Router,
    private activateroute:ActivatedRoute,
    private modalCtrl:ModalController
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res)=>{
      const obj=JSON.parse(res.pdfobj);
      this.pdfName=obj.category;
    });
 
    this.getPdfCategoryList();
  }


  async getPdfCategoryList(){

    const values= {
     "category":this.pdfName
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getPdfByCat',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.getPDFJson=response.data;

      }
      else if(response.result === false){
       this.config.loaderDismiss();
       
     }
      else{
       this.config.loaderDismiss();
       this.config.alertService('Something went wrong . Please try again later .');
      }
   },err=>{
    this.config.loaderDismiss();
    this.config.alertService('Something went wrong . Please try again later .');
   });

 }


 async openPdf(title,pdffile){

  const pdf=pdffile;

  const modalCreate=this.modalCtrl.create({
    component:ViewpdfComponentComponent,
    componentProps:{
      url:pdf,
      title:title
    }
  });

  await (await modalCreate).present();

 }

}
