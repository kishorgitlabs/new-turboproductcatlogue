import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.page.html',
  styleUrls: ['./pdf.page.scss'],
})
export class PdfPage implements OnInit {

getPDFJson=[];
  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() { 
 this.getPdfList();
  }


  async getPdfList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/getpdf').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.getPDFJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
         
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}

async gotoPdfDetails(a){

  const values={
    'category':a.category
  }

     let navigation:NavigationExtras={
       queryParams:{
         pdfobj:JSON.stringify(values)
       }
     };
     this.router.navigate(['/pdfdetails'],navigation)
}

}
