import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VidoepdfPage } from './vidoepdf.page';

const routes: Routes = [
  {
    path: '',
    component: VidoepdfPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VidoepdfPageRoutingModule {}
