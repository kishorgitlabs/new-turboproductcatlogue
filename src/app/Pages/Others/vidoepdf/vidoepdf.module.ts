import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VidoepdfPageRoutingModule } from './vidoepdf-routing.module';

import { VidoepdfPage } from './vidoepdf.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VidoepdfPageRoutingModule,
    ComponentModule
  ],
  declarations: [VidoepdfPage]
})
export class VidoepdfPageModule {}
