import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.page.html',
  styleUrls: ['./videos.page.scss'],
})
export class VideosPage implements OnInit {

  getVideoJson=[];

  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
   this.getVideoList();
  }

  async getVideoList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/getVideos').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.getVideoJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
         
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}


async gotoVideoDetails(a){

  const values={
    'category':a.category
  }

     let navigation:NavigationExtras={
       queryParams:{
         pdfobj:JSON.stringify(values)
       }
     };
     this.router.navigate(['/videodetails'],navigation)
}

}


