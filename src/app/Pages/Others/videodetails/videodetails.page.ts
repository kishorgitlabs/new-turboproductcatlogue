import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-videodetails',
  templateUrl: './videodetails.page.html',
  styleUrls: ['./videodetails.page.scss'],
})
export class VideodetailsPage implements OnInit {

  getVideoJson=[];
  videoName:string;


  constructor(
    private config:ConfigService,
    private router:Router,
    private activateroute:ActivatedRoute,
    private iab: InAppBrowser
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res)=>{
      const obj=JSON.parse(res.pdfobj);
      this.videoName=obj.category;
    });
   
    this.getVideoCategoryList();
  }


  async getVideoCategoryList(){

    const values= {
     "category":this.videoName
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getVideosByCat',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.getVideoJson=response.data;

      }
      else if(response.result === false){
       this.config.loaderDismiss();
       
     }
      else{
       this.config.loaderDismiss();
       this.config.alertService('Something went wrong . Please try again later .');
      }
   },err=>{
    this.config.loaderDismiss();
    this.config.alertService('Something went wrong . Please try again later .');
   });

 }



 videoFn(url) {
  const options: InAppBrowserOptions = {
    location: 'no',
    clearcache: 'yes',
    zoom: 'yes',
    toolbar: 'no',
    closebuttoncaption: 'back'
  };
  const browser: any = this.iab.create(url, '_blank', options);
}

}
