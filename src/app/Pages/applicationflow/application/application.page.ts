import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.page.html',
  styleUrls: ['./application.page.scss'],
})
export class ApplicationPage implements OnInit {

  ngSearchTerm;
  applicationJson=[];
  getOemName:string;


  constructor(
    private config:ConfigService,
    private router:Router,
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      const obj = JSON.parse(params.makeObj);
      this.getOemName=obj.make
   });

    this.getApplication();
  }

  async getApplication(){

    this.config.loader('Loading...');
    const values= {
      "oem_name":this.getOemName
     }
     this.config.postData('api/mobile/getMakeapplication1',values).subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          
        this.config.loaderDismiss();
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.applicationJson=response.data;
        }
        else if(response.result === false){
         this.config.loaderDismiss();
         
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}



async gotoEngineSpecs(application){
  this.config.logDetails('','',application,'','')

const values={
  applicationName:application,
  make:this.getOemName
}
let navigation:NavigationExtras={
  queryParams:{
    applicatonObj:JSON.stringify(values)
  }
};
this.router.navigate(['/applicationenginespecs'],navigation);

}

}
