import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-applicationenginespecs',
  templateUrl: './applicationenginespecs.page.html',
  styleUrls: ['./applicationenginespecs.page.scss'],
})
export class ApplicationenginespecsPage implements OnInit {

  getApplicationName :string;
  applicationListJson=[];
  isApplicationtable:boolean=false;
  oemName:string;
  constructor(
    private activatedRoute:ActivatedRoute,
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      const obj = JSON.parse(params.applicatonObj);
      this.getApplicationName=obj.applicationName,
      this.oemName=obj.make
   });
this.getEngineSpecs();
  }


  async getEngineSpecs(){

    const values= {
     "application":this.getApplicationName
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getApplicationPart',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.applicationListJson=response.data;
       this.isApplicationtable=true;
      }
      else if(response.result === false){
       this.config.loaderDismiss();
       
     }
      else{
       this.config.loaderDismiss();
       this.config.alertService('Something went wrong . Please try again later .');
      }
   },err=>{
    this.config.loaderDismiss();
    this.config.alertService('Something went wrong . Please try again later .');
   });

 }

 async gotoSegmentPartDetails(applicationdetails){

  this.config.logDetails('','',this.getApplicationName,applicationdetails.newpartno,'')
  const values={
    oldPartNo:applicationdetails.oldpartno,
    newPartNo:applicationdetails.newpartno,
    oemPartNo:applicationdetails.oempartno,
    engine:applicationdetails.engine,
    applicationName:this.getApplicationName,
    oemname:this.oemName

  }
  let navigation:NavigationExtras={
    queryParams:{
      engineObj:JSON.stringify(values)
    }
  };
  this.router.navigate(['/applicationpartdetails'],navigation)

 }

}
