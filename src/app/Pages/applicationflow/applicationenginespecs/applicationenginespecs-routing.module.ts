import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplicationenginespecsPage } from './applicationenginespecs.page';

const routes: Routes = [
  {
    path: '',
    component: ApplicationenginespecsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationenginespecsPageRoutingModule {}
