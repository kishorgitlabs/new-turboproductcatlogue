import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApplicationenginespecsPageRoutingModule } from './applicationenginespecs-routing.module';

import { ApplicationenginespecsPage } from './applicationenginespecs.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApplicationenginespecsPageRoutingModule,
    ComponentModule
  ],
  declarations: [ApplicationenginespecsPage]
})
export class ApplicationenginespecsPageModule {}
