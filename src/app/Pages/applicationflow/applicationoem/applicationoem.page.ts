import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-applicationoem',
  templateUrl: './applicationoem.page.html',
  styleUrls: ['./applicationoem.page.scss'],
})
export class ApplicationoemPage implements OnInit {

  
  ngSearchTerm;
  makeListJson:any =[];

  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getMakeList();
  }

  async getMakeList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/getMake').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
         
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.makeListJson=response.data;
   }
        else if(response.result === false){
         this.config.loaderDismiss();
       
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}



async gotoRemanSegment(a){
  
  this.config.logDetails(a.make,'','','','')
  const values={
    make:a.make
  }
 
  let navigation:NavigationExtras={
    queryParams:{
      makeObj:JSON.stringify(values)
    }
  };

  this.router.navigate(['/application'],navigation);

}


}
