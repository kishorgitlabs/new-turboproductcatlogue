import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApplicationoemPageRoutingModule } from './applicationoem-routing.module';
import { ComponentModule } from 'src/app/Component/Component.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ApplicationoemPage } from './applicationoem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApplicationoemPageRoutingModule,
    ComponentModule,
    Ng2SearchPipeModule
  ],
  declarations: [ApplicationoemPage]
})
export class ApplicationoemPageModule {}
