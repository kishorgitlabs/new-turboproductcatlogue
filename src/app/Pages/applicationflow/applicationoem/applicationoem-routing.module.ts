import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplicationoemPage } from './applicationoem.page';

const routes: Routes = [
  {
    path: '',
    component: ApplicationoemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationoemPageRoutingModule {}
