import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ImagemodalcomponentComponent } from 'src/app/Component/imagemodalcomponent/imagemodalcomponent.component';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-applicationpartdetails',
  templateUrl: './applicationpartdetails.page.html',
  styleUrls: ['./applicationpartdetails.page.scss'],
})
export class ApplicationpartdetailsPage implements OnInit {

  applicationPartDetailsJson = [];
  getOldPartNo: string;
  getOemPartNo: string;
  getEngineName: string;
  getApplication: string;
  getMakeName: string;
  getNewPartNo: string;
  partdetails: string;
  userDetail: any;
  getUserType: string;
  getMobleNo: string;
  type: string = 'Specification';
  isKitDetails:boolean=true;
  isServiceInformation:boolean=true;
  isTel:boolean=false;
  oemname:string;
  constructor(
    private modalCtrl: ModalController,
    private activateroute: ActivatedRoute,
    private config: ConfigService
  ) { }

  ngOnInit() {
    this.userDetail = JSON.parse(localStorage.getItem('UserDetail'));

    this.getMobleNo = this.userDetail.mobileno;
    this.getUserType = this.userDetail.usertype;


   if(this.getUserType === 'End User' ){
     this.isKitDetails=false;
     this.isServiceInformation=false;
     
   }

   if(this.getUserType === 'TEL Employee' || this.getUserType === 'ASC'){
    this.isTel=true;
      }

   if(this.getUserType === 'Distributor'){
    
    this.isServiceInformation=false;
  }
    this.activateroute.queryParams.subscribe((res) => {

      const obj = JSON.parse(res.engineObj);
      this.getOldPartNo = obj.oldPartNo;
      this.getOemPartNo = obj.oemPartNo;
      this.getEngineName = obj.engine;
      this.getApplication = obj.applicationName;
      this.getNewPartNo = obj.newPartNo;
      this.getMakeName=obj.oemname
    });
    this.getMakeApplicationPartDetails();
  }
  async openPreview(a) {
    const imgURL = a;
    const modal = await this.modalCtrl.create({
      component: ImagemodalcomponentComponent,
      cssClass: 'transparent-modal',
      componentProps: {
        img: imgURL
      }
    });
    modal.present();
  }


  async getMakeApplicationPartDetails() {

    this.config.logDetails('','','',this.getNewPartNo,this.getApplication)

    const values = {
      "tca_newpartno": this.getNewPartNo,
      "oem_partnumber": this.getOemPartNo,
      "engine": this.getEngineName,
      "application": this.getApplication,
      "mobileno": this.getMobleNo,
      "usertype": this.getUserType
    }

    this.config.loader('Loading...');
    this.config.postData('api/mobile/getMakeApplicationParDetails', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {

        this.config.loaderDismiss();
        this.applicationPartDetailsJson = response.data;

      }
      else if (response.result === false) {
        this.config.loaderDismiss();
    

      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');

      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');

    });

  }


}
