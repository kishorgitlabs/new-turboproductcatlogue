import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApplicationpartdetailsPageRoutingModule } from './applicationpartdetails-routing.module';

import { ApplicationpartdetailsPage } from './applicationpartdetails.page';
import { ComponentModule } from 'src/app/Component/Component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApplicationpartdetailsPageRoutingModule,
    ComponentModule
  ],
  declarations: [ApplicationpartdetailsPage]
})
export class ApplicationpartdetailsPageModule {}
