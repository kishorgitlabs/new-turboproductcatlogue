import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplicationpartdetailsPage } from './applicationpartdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ApplicationpartdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationpartdetailsPageRoutingModule {}
