import { ComponentModule } from 'src/app/Component/Component.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchkitnoPageRoutingModule } from './searchkitno-routing.module';

import { SearchkitnoPage } from './searchkitno.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchkitnoPageRoutingModule,
    ComponentModule
  ],
  declarations: [SearchkitnoPage]
})
export class SearchkitnoPageModule {}
