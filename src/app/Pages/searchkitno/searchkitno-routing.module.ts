import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchkitnoPage } from './searchkitno.page';

const routes: Routes = [
  {
    path: '',
    component: SearchkitnoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchkitnoPageRoutingModule {}
