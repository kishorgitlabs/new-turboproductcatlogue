import { LoginComponent } from './../../Component/login/login.component';
import { Router } from '@angular/router';
import { ConfigService } from './../../Services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, Platform } from '@ionic/angular';
declare var window;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  UserDetail: any;
  name: string;
  usertype: string;
  isKitNo: boolean = true;
  kitName: string = '';
  slidesOptions = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true
  };
  constructor(
    private modalctrl: ModalController,
    private config: ConfigService,
    private router: Router,
    private platform: Platform,
    private alertCtrl: AlertController
  ) { }

  ionViewWillEnter() {
    const lsAndroidVersion = localStorage.getItem('lsAppVersion');
    console.log('47',lsAndroidVersion);
    if (lsAndroidVersion === null) {
      this.config.relogInFn();
    }
  }

  ngOnInit() {
    window.app.callMethod();
    this.UserDetail = JSON.parse(localStorage.getItem('UserDetail'));
    console.log(this.UserDetail);
    this.usertype = this.UserDetail.usertype;
    if (this.usertype === 'End User') {
      this.isKitNo = false;
    }
    if (this.usertype === 'Distributor') {
      this.kitName = 'Search by overhaul kit / back plate partno'
    }
    if (this.usertype === 'ASC' || this.usertype === 'TEL Employee') {
      this.kitName = 'Search by overhaul kit / core assembly kit partno'
    }
    this.checkStatus();
    this.platform.backButton.subscribe(() => {
      if (this.router.url === '/dashboard') {
        navigator['app'].exitApp();
      }
    });
  }

  async checkStatus() {
    const values = {
      "mobileno": this.UserDetail.mobileno
    };
    this.config.loader('Loading...');
    this.config.postData('api/mobile/usercheck', values).subscribe((res) => {
      let response: any = res;
      console.log(response.result);
      if (response.result === true) {
        this.config.loaderDismiss();
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        this.config.alertService('Your account is deactivated . Please contact turbo team .');
      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
      this.config.loaderDismiss();
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });
  }


  async checkLogin(pageUrl) {
    if (this.usertype === 'End User') {
      this.router.navigateByUrl(`/${pageUrl}`);
      return;
    }
    if (localStorage.getItem('lsLogin') === null) {
      const modalCreate = this.modalctrl.create({
        component: LoginComponent,
        backdropDismiss: true
      });
      await (await modalCreate).present();
    }
    else {
      this.router.navigateByUrl(`/${pageUrl}`);
    }

  }



}
