import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfigService } from 'src/app/Services/config/config.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.page.html',
  styleUrls: ['./contactus.page.scss'],
})
export class ContactusPage implements OnInit {
  type: string;
  stateListJson=[];
  states=[];
  getCityJson=[];
  getContactJson=[];
  isContactList:boolean=false;
  stateModel:any;
  cityModel:any;
  UserDetail: any;
  name:string;
  mobileno:string;
  usertype:string;

  message:string;
  subject:string;
  contacttype:string;

  ContactTypeArr = [
    'Enquiry',
    'Feedback'
  ];
  feedbackForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private config:ConfigService
  ) {
    this.feedbackForm = new FormGroup({
      contactType: new FormControl(null, [Validators.required]),
      subject: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required])
    });
   }

  ngOnInit() {

    this.UserDetail = JSON.parse(localStorage.getItem('UserDetail'));
  
    this.name=this.UserDetail.username;
    this.mobileno=this.UserDetail.mobileno;
    this.usertype=this.UserDetail.usertype

    this.type = 'Customer Support Engineer';
    this.getStateList();
  }


  async getStateList(){

    const values = {
      "type": 'Customer Support Engineer'
    };

    this.config.loader('Loading...');
     this.config.postData('api/mobile/statelist',values).subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.stateListJson=response.data;
         
   }
        else if(response.result === false){
         this.config.loaderDismiss();
         
       }
        else{
          this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
        }
     },err=>{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
     });

}


async getCityList(obj){
  this.cityModel=null;
  const values= {
   "state":obj.state
  }

 this.config.loader('Loading...');
 this.config.postData('api/mobile/citylist',values).subscribe((res)=>{

    const response:any =res;

    if(response.result === true){
     this.config.loaderDismiss();
    
this.getCityJson=response.data;
    }
    else if(response.result === false){
     this.config.loaderDismiss();
     
   }
    else{
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    }
 },err=>{
  this.config.loaderDismiss();
          this.config.alertService('Something went wrong . Please try again later .');
 });

}


async getContactDetails(){
  if(this.stateModel === ''  || this.stateModel === undefined || this.stateModel === null){
    this.config.toastfn('Select any state');
    return;
  } 
  const values = {
    "type": "Customer Support Engineer",
    "state":this.stateModel,
    "city":this.cityModel
  }

 console.log(this.stateModel);
 console.log(this.cityModel);
 

  this.config.loader('Loading...');
  this.config.postData('api/mobile/network', values).subscribe((res) => {
    let response: any = res;
    if (response.result === true) {
      this.config.loaderDismiss();
  this.isContactList=true;
     this.getContactJson=response.data;
    }
    else if (response.result === false) {
      this.config.loaderDismiss();
     this.config.alertService('No Data Found .')  
    }
    else {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    }
  }, err => {
    this.config.loaderDismiss();
    this.config.alertService('Something went wrong . Please try again later .');
  });
}



async getContactList(){
  if(this.stateModel === ''  || this.stateModel === undefined || this.stateModel === null){
    this.config.toastfn('Select any state');
    return;
  } 
this.getContactDetails();
  
}

  onFormSubmit() {
    if (!this.feedbackForm.valid) {
      this.feedbackForm.markAllAsTouched();
      return;
    }
    

    if(this.contacttype === '' || this.contacttype === undefined || this.contacttype === null){
      this.config.toastfn('Select any contacttype');
      return
    }
    if(this.message === '' || this.message === undefined || this.message === null){
      this.config.toastfn('Enter any message');
      return
    }
    if(this.subject === '' || this.subject === undefined || this.subject === null){
      this.config.toastfn('Enter any subject');
      return
    }

    const values = {
      "Mobileno": this.mobileno,
      "Name":this.name,
      "usertype":this.usertype,
      "contacttype":this.contacttype,
      "message":this.message,
      "subject":this.subject
    }
  

  
    this.config.loader('Loading...');
    this.config.postData('api/mobile/sendcontactus', values).subscribe((res) => {
      let response: any = res;
      if (response.result === true) {
        this.config.loaderDismiss();
         this.config.alertService('Feedback has been sent to turbo team .');
         this.feedbackForm.reset();
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
  
      }
      else {
        this.config.loaderDismiss();
        this.config.alertService('Something went wrong . Please try again later .');
      }
    }, err => {
      this.config.loaderDismiss();
      this.config.alertService('Something went wrong . Please try again later .');
    });





  }
  get _v() {
    return this.feedbackForm.value;
  }



}
