import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController, ModalController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AlertComponent } from 'src/app/Component/alert/alert.component';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  public rootUrl = 'https://productcatalogjson.turboenergy.co.in/';
  // public rootUrl = 'http://122.165.54.203:8001/turboproductcatalogjson/';
  // public rootUrl = 'https://brainmagicllc.com/turboAPI/';
  isLoading: boolean = false;
  name: string;
  mobileno: string;
  usertype: string;
  devicetype: string;
  state: string;
  city: string;
  zipcode: string;
  UserDetail: any;
  constructor(
    private http: HttpClient,
    private modalctrl: ModalController,
    private loadctrl: LoadingController,
    private taostctrl: ToastController,
    private alertController: AlertController,
    private menuCtrl: MenuController,
    private route: Router
  ) { }


  getData(url): Observable<any> {
    const geturl = `${this.rootUrl}${url}`;
    return this.http.get(geturl);
  }

  postData(url, values): Observable<any> {
    const posturl = `${this.rootUrl}${url}`;
    return this.http.post(posturl, values);
  }

  async alertService(msg) {

    const modalCreate = this.modalctrl.create({
      component: AlertComponent,
      cssClass: 'alertclass',
      backdropDismiss: false,
      componentProps: {
        'alertName': msg
      }
    });
    return (await modalCreate).present();
  }
  async networkExit() {
    navigator['app'].exitApp();
  }

  async loader(msg) {
    this.isLoading = true;
    return await this.loadctrl.create({
      spinner: 'crescent',
      message: msg,
      duration: 25000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async loaderDismiss() {
    this.isLoading = false;
    return await this.loadctrl.dismiss().then(() => console.log('dismissed'));
  }

  async toastfn(msg) {
    const toastCreate = this.taostctrl.create({
      message: msg,
      position: 'bottom',
      duration: 3000,
      buttons: [
        {
          text: 'Okay'
        }
      ]
    });
    return await (await toastCreate).present();
  }


  async logDetails(make, segment, subsegment, partno, model) {
    this.UserDetail = JSON.parse(localStorage.getItem('UserDetail'));
    const url = `${this.rootUrl}/api/mobile/LogDetailNew`;
    const values = {
      "Make": make,
      "Segment": segment,
      "SubSegment": subsegment,
      "Partno": partno,
      "VehicleModel": model,
      "Name": this.UserDetail.username,
      "Mobileno": this.UserDetail.mobileno,
      "Usertype": this.UserDetail.usertype,
      "Devicetype": this.UserDetail.devicetype,
      "State": this.UserDetail.state,
      "City": this.UserDetail.city,
      "Location": '',
      "Zipcode": this.UserDetail.Pincode
    };
    this.http.post(url, values).subscribe((res) => {
      console.log(res);
    }, err => {
      console.log(err);
    });
  }

  getNADetails(value) {
    console.log(value);
    if (value === "" || value === null || value === undefined) {
      return "N/A";
    } else {
      return value;
    }
  }

  async relogInFn() {
    const alert = await this.alertController.create({
      header: 'Login',
      message: 'Please relogin again',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            // this.menuCtrl.close();
            this.menuCtrl.enable(false);
            localStorage.clear();
            this.route.navigate(['/register']);
          }
        }
      ]
    });
    await alert.present();
  }

}
