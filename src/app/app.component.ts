import { Component } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { AlertController, MenuController, ModalController, Platform } from '@ionic/angular';
import { ConfigService } from './Services/config/config.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Router } from '@angular/router';
import { LoginComponent } from './Component/login/login.component';
declare var window;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  public appPages = [
    { title: 'Home', url: '/dashboard', icon: 'home' },
    { title: 'Manufacturer', url: '/make', icon: 'car-sport' },
    { title: 'Segment', url: '/segment', icon: 'bus' },
    { title: 'Application', url: '/application', icon: 'document' },
    { title: 'Price List', url: '/pricelists', icon: 'pricetags' },
    { title: 'Network', url: '/network', icon: 'git-network' },
    // { title: 'Search', url: '/search', icon: 'search' },
    { title: 'Profile', url: '/editprofile', icon: 'settings' }
  ];


  UserDetail: any;
  usertype: string;
  constructor(
    private platform: Platform,
    private network: Network,
    private alertCtrl: AlertController,
    private screenOrientation: ScreenOrientation,
    private config: ConfigService,
    private menuCtrl: MenuController,
    private router: Router,
    private modalctrl: ModalController
  ) {
    window.app = this;
    this.platform.ready().then(async (res) => {
      this.UserDetail = JSON.parse(localStorage.getItem('UserDetail'));
      this.usertype = this.UserDetail.usertype;
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.network.onConnect().subscribe(() => {
        setTimeout(() => {
        }, 3000);
      })
      this.network.onDisconnect().subscribe(() => {
        this.config.alertService('Network Disconnected');
      });
    });
  }
  

  callMethod() {
    if (localStorage.getItem('UserDetail') != null) {
      this.UserDetail = JSON.parse(localStorage.getItem('UserDetail'));
      this.usertype = this.UserDetail.usertype;
    }
  }


  async checkLogin(pageUrl, pagename) {

    if (this.usertype === 'End User' || this.usertype === undefined) {
      this.router.navigateByUrl(`/${pageUrl}`);
      return;
    }

    if (pagename === 'Home' || pagename === 'Settings') {
      this.router.navigateByUrl(`/${pageUrl}`);
      return;
    }

    if (localStorage.getItem('lsLogin') === null && this.usertype !== 'End User') {
      const modalCreate = this.modalctrl.create({
        component: LoginComponent
      });
      await (await modalCreate).present();
    }
    else {
      this.router.navigateByUrl(`/${pageUrl}`)
    }
  }
}
function errorCallback(error: any) {
  throw new Error('Function not implemented.');
}

